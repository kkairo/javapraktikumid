package proovimine;

import java.util.Arrays;

public class mediaan {

	public static void main(String[] args) {

		int[][] matrix = new int[][] { { 10, 12, 21, 51, 41, 38, 8 }, { 13, 200, 43, 44, 100, 150 }, { 1, 2, 3, 4 },
				{ 1, 2, 3 } };
				// System.out.println(matrix[1][0]);

		// Defineerime uue maatrksi sortMatrix, kopeerime sellesse algmaatriksi
		// sisu ja sorteerimine nii, et k�ikidel ridadel on elemendid suuruse
		// j�rjekorras
		int[][] sortMatrix = new int[matrix.length][];
		for (int i = 0; i < matrix.length; i++)
			sortMatrix[i] = matrix[i].clone();
		// sorteerimine
		for (int i = 0; i < sortMatrix.length; i++) {
			Arrays.sort(sortMatrix[i]);
		}
		
		
//		  double[][] doublearray = new double[intarray.length][intarray[0].length];
//
//		    for(int i = 0; i < intarray.length; i++)
//		    {
//		        for(int j = 0; j < intarray[0].length; j++)
//		            doublearray[i][j] = (double) intarray[i][j];
//		    }
		
		// Defineerime double t��pi massiivi, kuhu paneme k�ikide ridade
		// mediaanv��rtused. Paaritu arvu elementide korral sorteerirud rea
		// mediaan v��rtus keskmine element (v��rtus kohal keskarv =
		// sortMatrix.length/2). Paarisarvu elementide korral tuleb mediaani
		// leidmiseks liita elemendid kohal keskArv-1 ja keskArv ning jagada
		// nende summa kahega, st mediaan ei pruugi olla enam t�isarv.
		// Sellep�rast peabki mediaanidest koosneva massiiv t��p olema double
		double[] mediaanid = new double[sortMatrix.length];
		for (int i = 0; i < sortMatrix.length; i++) {
			int keskArv = sortMatrix[i].length / 2;
			if (sortMatrix[i].length % 2 != 0) {
				mediaanid[i] = sortMatrix[i][keskArv];
			} else {
				mediaanid[i] = (sortMatrix[i][keskArv - 1] + sortMatrix[i][keskArv]) / 2 + 0.5;
			}
		}
		// for (double m : mediaanid) {
		// System.out.print(m + "\t");
		// }

		double max = mediaanid[0];
		int[] medInt = new int[mediaanid.length];
		for (int i = 0; i < mediaanid.length; i++) {
			if (mediaanid[i] > max) {
				for (double m : matrix[i]) {
					System.out.print(m + "\t");
				}
			}
		}
		// for (int i = 0; i < medInt.length; i++)
		// medInt[i] = (int) [i];

		for (int m : medInt) {
			System.out.println(m);
		}

	}

}
