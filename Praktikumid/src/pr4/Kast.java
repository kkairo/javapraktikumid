package pr4;

public class Kast {

	public static void main(String[] args) {

		// Kasti suuruse defineerimine
		int d = 1100;

		for (int i = 1; i <= d; i++) {
			for (int j = 1; j <= d; j++) {
				
				// Kriteeriumid, kunas on v��rtuseks "x", "|" v�i "0" 
				if (i > 1 && i < d && j > 1 && j < d && (i == j || d - i + 1 == j)) {
					System.out.print("x ");
				} else if (i == 1 || i == d) {
					System.out.print("- ");
				} else if (j == 1 || j == d) {
					System.out.print("| ");
				} else {
					System.out.print("0 ");
				}
			}
			// Ridade vahetamiseks
			System.out.println();
		}
	}
}