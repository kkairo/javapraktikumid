package pr6;

public class Meetodid {

	public static void main(String[] args) {
		
		int summa = summa(new int[] {4, 3, 1, 7, -1});
	}

	// * Kuubi arvutamise meetod

	public static int arvuKuup(int arv) {

		int arvKuubis = (int) Math.pow(arv, 3);
		return arvKuubis;

	}

	// * Arvu kontrollimine vahemikus meetod (variant 1)

	public static int sisestuseVahemikuKontroll(String error, int min, int max) {

		int sisendArv; // do-while ts�kli korral peab miskip�rast muutujad enne
						// ts�klit �ra m��rama
		do {
			sisendArv = TextIO.getlnInt();
			if (sisendArv < min || sisendArv > max) {
				System.out.println(error);
			}
		} while (sisendArv < min || sisendArv > max);
		return sisendArv;

	}

	// * Arvu kontrollimine vahemikus meetod (variant 2)

	// public static int sisestuseVahemikuKontroll(String k2sk, int min, int
	// max) {
	//
	// while (true) {
	// System.out.print(k2sk);
	// int sisestajaArv = TextIO.getlnInt();
	// if (sisestajaArv >= min && sisestajaArv <= max) {
	// return sisestajaArv;
	// }
	// }
	//
	// }

	// * Suvalise arvu mingis vahemikus genereermise meetod

	public static int suvalineT2isarvVahemikus(int min, int max) {
		int suvaline = min + (int) (Math.random() * ((max - min) + 1));
		return suvaline;

	}

	// * Numbrite summa massiivis
	public static int summa(int[] massiiv) {
		int summa = 0;
		for (int i = 0; i < massiiv.length; i++){
			summa = summa + massiiv[i];
		}
		System.out.println(summa);
		return summa;

	}

}
