package pr6;

public class ArvuArvamine {

	public static void main(String[] args) {

		// Suvalise arvu genereemise meetodi v�ljakutsumine. PS! v�lja kutsutud
		// pr6-st, mitte pr5-st, seega ka pisut erinev
		int suvaline = Meetodid.suvalineT2isarvVahemikus(1, 100);

		System.out.println("Arva �ra, mis arvu arvuti m�tleb: ");
		// System.out.println(suvaline);

		int pakkumine;
		
		// While-do ts�kkel k�sib kasutajalt arvu seni, kui see on �ige
		do {
			pakkumine = TextIO.getlnInt();

			if (pakkumine > suvaline) {
				System.out.println("Pakkumine liiga suur. Arva uuesti");
			} else if (pakkumine < suvaline) {
				System.out.println("Pakkumine liiga v�ike. Arva uuesti");
			}

		} while (suvaline != pakkumine);

		// �ige arvu pakkumisel programm l�ppeb
		if (suvaline == pakkumine) {
			System.out.println("Palju �nne! Arvasid �ra. See number on " + suvaline + ".");
		}
	}

}
