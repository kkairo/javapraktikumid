package pr6;

public class T2ringuViskamine {

	public static void main(String[] args) {

		// * T�pselt sama m�ng, mis m�ndiviskamisega, ainult juhuslik arv
		// genereeritakse vahemikus 1-6 ja �ige arvamise korral tagastatakse
		// 6-kordne panus

		int pakkumine;
		int raha;
		int panus;
		raha = 100;

		do {

			// t�ringuveeretamine
			int t2ring = Meetodid.suvalineT2isarvVahemikus(1, 6);

			// panuse k�simine ja kontrollimine
			System.out.println("Sisesta panus (max 25 punkti): ");
			panus = TextIO.getlnInt();
			while (panus > 25 || panus > raha) {
				if (panus > 25) {
					System.out.println("Liiga suur panus. Sisesta v�iksem panus.");
				} else if (panus > raha && raha < 25) {
					System.out.println("Sul ei ole nii palju raha. Sisesta v�iksem panus.");
				}
				panus = TextIO.getlnInt();
			}

			// numbri �ra arvamine
			System.out.println("Arva �ra, mis number veeretatakse.");
			pakkumine = Meetodid.sisestuseVahemikuKontroll("Sisesta arv 1 - 6.", 1, 6);

			// �ige pakkumine
			if (pakkumine == t2ring) {
				raha += 6 * panus;
				System.out.println("Arvasid �igesti ja sul on n��d " + raha + " punkti.");
			}

			// vale pakkumine
			else if (pakkumine != t2ring) {
				raha -= panus;
				System.out.println("Arvasid valesti ja sul on n��d " + raha + " punkti.");
			}

		} while (raha > 0);

		if (raha <= 0) {
			System.out.println("Punktid said otsa. M�ng on l�bi");
		}
	}

}
