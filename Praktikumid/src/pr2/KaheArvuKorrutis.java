package pr2;

public class KaheArvuKorrutis {
	public static void main(String[] args) {

		System.out.print("Sisesta esimene arv: ");
		double arv1 = TextIO.getlnDouble();
		System.out.print("Sisesta teine arv: ");
		double arv2 = TextIO.getlnDouble();

		double korrutis = arv1 * arv2;
		System.out.print("Nende arvude korrutis on " + korrutis);

	}
}
