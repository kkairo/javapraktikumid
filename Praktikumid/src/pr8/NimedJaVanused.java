package pr8;

import java.util.ArrayList;

public class NimedJaVanused {

	// Kirjutada programm, mis k�sib kasutajalt vaheldumisi inimeste nimesid ja
	// vanuseid. Kasutaja sisestatud andmed pane n�iteprogrammide hulgas
	// olevasse "Inimene" klassi. Kutsu �kshaaval v�lja iga "Inimene" t��pi
	// objekti tervita() - meetod.

	public static void main(String[] args) {

		String nimi = "";
		int vanus = 0;
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		while (true) {

			System.out.print("Sisesta nimi: ");
			nimi = TextIO.getlnString();
			if (nimi.equals("exit")) {
				break;
			}
			System.out.print("Sisesta selle inimese vanus: ");
			vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));
		}
		
		for (Inimene inimene : inimesed) {
			// Java kutsub v�lja Inimene klassi toString() meetodi
			System.out.println(inimene);

		}
		// Inimene lause = new tervita();
		
		// ei oska teha, ei saa �lesandest aru
		
	}

}
