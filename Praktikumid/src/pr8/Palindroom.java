package pr8;

public class Palindroom {
	
	// Kirjutada programm, mis k2seb kasutajalt s6na ja kontrollib, kas see
	// on edaspidi ja tagurpidi lugedes sama (palindroom).

	public static void main(String[] args) {


		// Sõna sisestamine
		System.out.print("Sisesta s�na: ");
		String s6na = TextIO.getlnString();

		// Sõna tagurpidi p��ramine (PS! tagurpidi string peab defineerima enne
		// for-tsyklit kui t�hja stringina
		String tagurpidi = "";
		for (int i = s6na.length() - 1; i >= 0; i--) {
			tagurpidi = tagurpidi + s6na.charAt(i);
		}

		// S6na ja tagurpidi s6na v6rdlemine
		if (s6na.equals(tagurpidi)) {
			System.out.println("S�na " + s6na + " on palindroom!");
		} else {
			System.out.println("S�na " + s6na + " ei ole palindroom!");
		}

	}

}