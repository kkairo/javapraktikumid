package pr5;

public class Tellikivi {

	public static void main(String[] param) {

		// Kivi m��tmed (n�iteks)
		double a = 1000, b = 20, c = 5;

		// Augu m��tmed
		double x = 30, y = 40;

		// Meetodi v�ljakutsumine
		boolean kasMahub = mahub(a, b, c, x, y);
		System.out.println(kasMahub);

	}

	// Meetodina ise (nii nagu moodles peaks esitama)
	public static boolean mahub(double a, double b, double c, double x, double y) {

		// IDEE: v�hemalt kaks tellise serva peavad olema l�hemad, kui kumbki
		// augu serv
		if ((a <= x && b <= y) || (a <= y && b <= x) || (a <= x && c <= y) || (a <= y && c <= x) || (b <= x && c <= y)
				|| (b <= y && c <= x)) {
			return true;
		} else {
			return false;
		}
	}

}
