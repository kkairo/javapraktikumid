package pr5;

public class Meetodid {

	public static void main(String[] args) {

	}

	// * Kuubi arvutamise meetod

	public static int arvuKuup(int arv) {

		int arvKuubis = (int) Math.pow(arv, 3);
		return arvKuubis;

	}

	// * Arvu kontrollimine vahemikus meetod (variant 1)

	public static int kasutajaSisestus(int min, int max) {

		int sisendArv; // do-while ts�kli korral peab miskip�rast muutujad enne ts�klit �ra m��rama
		do {
			System.out.print("Sisesta arv: ");
			sisendArv = TextIO.getlnInt();
		} while (sisendArv < min || sisendArv > max);
		System.out.println(sisendArv);
		return sisendArv;

	}

	// * Arvu kontrollimine vahemikus meetod (variant 2)

	// public static int kasutajaSisestus(int min, int max) {
	//
	// while (true) {
	// System.out.print("Sisesta arv: ");
	// int sisestajaArv = TextIO.getlnInt();
	// if (sisestajaArv >= min && sisestajaArv <= max) {
	// System.out.println(sisestajaArv);
	// return sisestajaArv;
	// }
	// }
	//
	// }

	// * Suvalise arvu mingis vahemikus genereermise meetod
	
	public static int suvalineT2isarvVahemikus(String kysimus, int min, int max) { 	//kui string ei taha kasutada sisesta ""

		int suvaline = min + (int) (Math.random() * ((max - min) + 1));
		System.out.print(kysimus);
		return suvaline;

	}

}
