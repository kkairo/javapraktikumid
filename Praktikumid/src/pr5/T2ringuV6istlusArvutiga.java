package pr5;

public class T2ringuV6istlusArvutiga {
	public static void main(String[] args) {

		// ** PARIM LAHENDUS ON L�PUS (nr 3)

		// * 1. Labane lahendus

		// Arvuti visked
		// int aEsimeneVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// int aTeineVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// int aViseteSumma = aEsimeneVise + aTeineVise;
		// System.out.println("Arvuti viskas " + aViseteSumma + " silma.");
		//
		// M�ngija visked
		// int mEsimeneVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// int mTeineVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// int mViseteSumma = mEsimeneVise + mTeineVise;
		// System.out.println("M�ngija viskas " + mViseteSumma + " silma.");
		//
		// if (aViseteSumma > mViseteSumma) {
		// System.out.println("Arvuti v�itis.");
		// }
		// else if (aViseteSumma == mViseteSumma) {
		// System.out.println("Viik.");
		// else {
		// System.out.println("M�ngija v�itis.");
		// }

		// * 2. Kahe for loopiga lahendus. Esitakse arvuti kaks vistet j�rjest
		// ja
		// siis esitatakse m�ngija kaks viset j�rjest

		// int aViseteSumma = 0;
		// int mViseteSumma = 0;
		// for (int i = 1; i <= 2; i++) {
		// int aVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// aViseteSumma += aVise;
		// System.out.println("Arvuti " + i + ". vise oli " + aVise);
		// }
		// for (int i = 1; i <= 2; i++) {
		// int mVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
		// mViseteSumma += mVise;
		// System.out.println("M�ngija " + i + ". vise oli " + mVise);
		// }
		//
		// System.out.println("Arvuti poolt visatud silmade summa oli " +
		// aViseteSumma);
		// System.out.println("M�ngija poolt visatud silmade summa oli " +
		// mViseteSumma);
		//
		// if (aViseteSumma > mViseteSumma) {
		// System.out.println("Arvuti v�itis.");
		// }
		// else if (aViseteSumma == mViseteSumma) {
		// System.out.println("Viik.");
		// }
		// else {
		// System.out.println("M�ngija v�itis.");
		// }

		// * 3. �he for loopiga lahendus. Arvuti ja m�ngija visete tulemused
		// esitatakse kordam��da (mis ei oma tegelt t�htsust)

		// Selguse m�ttes defineerime m�lemad, nii arvuti kui ka m�ngija simade
		// summa enne viskamist (need on nullid). a t�histab arvutit, m m�ngjat
		int aViseteSumma = 0;
		int mViseteSumma = 0;

		// Summade arvutamiseks kasutame for ts�klit ja kutsume v�lja meetodi
		// "suvalineT2isarvVahemikus"
		for (int i = 1, j = 1; i <= 2; i++, j++) {

			int aVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
			aViseteSumma += aVise;
			// Tr�kitakse �ksiku t�ringu silmade arvu ( ei pea tegelt v�lja
			// printima )
			System.out.println("Arvuti " + i + ". vise oli " + aVise);

			int mVise = Meetodid.suvalineT2isarvVahemikus("", 1, 6);
			mViseteSumma += mVise;
			// Tr�kitakse �ksiku t�ringu silmade arvu ( ei pea tegelt v�lja
			// printima )
			System.out.println("M�ngija " + j + ". vise oli " + mVise);

		}

		// Silmade summa v�ljatr�kkimine
		System.out.println("Arvuti poolt visatud silmade summa oli " + aViseteSumma);
		System.out.println("M�ngija poolt visatud silmade summa oli " + mViseteSumma);

		// V�itja v�ljaselgitamine
		if (aViseteSumma > mViseteSumma) {
			System.out.println("Arvuti v�itis.");
		} else if (aViseteSumma == mViseteSumma) {
			System.out.println("Viik.");
		} else {
			System.out.println("M�ngija v�itis.");
		}

	}

}// main
