package pr5;

public class KullV6iKiri {

	public static void main(String[] args) {

		// Kasutatud meetodit "suvalineT2isarvVahemikus", et arvutada, kas
		// arvuti viskab kull (0) v�i kirja (1)
		int arvuti = Meetodid.suvalineT2isarvVahemikus("Sisesta kull (0) v�i kiri (1): ", 0, 1);

		// While-do ts�kli jooksul k�sitakse m�ngijalt vastust. Ts�kkel kestab
		// seni, kuni sisend arv ei v�rdu arvuti omaga
		int sisendArv;
		do {
			sisendArv = TextIO.getlnInt();
			if (sisendArv != arvuti) {
				System.out.println("Arvuti viskas (" + arvuti + ") ehk sa arvasid valesti.");
			}
		} while (sisendArv != arvuti);

		// Kui sisesndarv v�rdub arvuti omaga, siis on m�ngija v�itnud
		if (sisendArv == arvuti) {
			System.out.println("Arvuti viskas (" + arvuti + ") ehk sa arvasid �igesti.");

		}

	}
}
