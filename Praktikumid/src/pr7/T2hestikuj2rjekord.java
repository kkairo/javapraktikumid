package pr7;

import java.util.ArrayList;
import java.util.Collections;

public class T2hestikuj2rjekord {

	public static void main(String[] args) {

		// Massiivi defineerimine
		ArrayList<String> nimed = new ArrayList<String>();

		// Nimede sisestamine
		while (true) {
			System.out.print("Sisesta nimi: ");
			String nimi = TextIO.getlnString();
			if (nimi.equals("")) {
				break;
			}
			nimed.add(nimi);
		}

		// Nimede tähestikujärjekorda seadmine ja esitamine
		Collections.sort(nimed);
		for (String nimi : nimed) {
			System.out.println(nimi);
		}

	}

}
