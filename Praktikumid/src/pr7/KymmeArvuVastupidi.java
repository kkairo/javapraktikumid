package pr7;

public class KymmeArvuVastupidi {

	public static void main(String[] args) {

		// Massiivi defineerimine
		int[] arvud = new int[10];

		// Kasutaja k�est arvude k�simine ja massiivi sisestamine
		for (int i = 0; i < arvud.length; i++) {
			int kasutajaArvud = TextIO.getlnInt();
			arvud[i] = kasutajaArvud;
		}

		// Kontroll (see on for each ts�kkel, sellega saab terve massiivi
		// lihtsalt v�lja tr�kkida)
		for (int arv : arvud) {
			System.out.print(arv + "  ");
		}
		// Massiivi sisu �mberp��ramine
		System.out.println();
		for (int i = arvud.length - 1; i >= 0; i--) {
			System.out.print(arvud[i] + "  ");
		}

	}

}
