package pr7;

public class Meetodid {

	public static void main(String[] args) {

		// Proovimaatriks
		int[][] proov = new int[][] { { 0, 0, -2000 }, { 0, 1, 4, 90 }, { 0, 0, 100 }, { -9, 0, 1 } };

		// Meetodi v�ljakutsumine
		System.out.println(maatriksMax(proov));

	}

	// * Maksimumi leidmine 1D massiivist

	public static int massiivMax(int[] massiiv) {

		int max = massiiv[0];
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i] > max) {
				max = massiiv[i];
			}
		}
		return max;
	}

	// * Maksimumi leidmine 2D massiivist (maatriksist)

	public static int maatriksMax(int[][] maatriks) {

		// Loome t�hja massiivi, kuhu hiljem paigutame iga rea maksimumi
		int[] ridadeMaksimumid = new int[maatriks.length];

		// For ts�kli abil leiame maatriksi iga rea maksimumv��rtuse ja lisame
		// need t�hja massiivi
		for (int i = 0; i < maatriks.length; i++) {
			ridadeMaksimumid[i] = massiivMax(maatriks[i]);
		}

		// Leiame maksimumide massiivist omakorda maksimumi
		int l6plikMaksimum = massiivMax(ridadeMaksimumid);
		return l6plikMaksimum;
	}

}
