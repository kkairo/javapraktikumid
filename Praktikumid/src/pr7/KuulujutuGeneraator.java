package pr7;

public class KuulujutuGeneraator {

	public static void main(String[] args) {

		// Massiivide defineerimine
		String naisenimed[] = new String[] { "Kai", "Kerli", "Piret" };
		String mehenimed[] = new String[] { "Aivars", "Jevgeni", "Jaak" };
		String tegus6nad[] = new String[] { "Loves", "Hates", "Kills" };

		// Massiivi suvlise indeksi genereerimine (meetodi v�lja kutsumine pr6
		// Meetodid)
		int suvalineNaine = pr6.Meetodid.suvalineT2isarvVahemikus(0, naisenimed.length - 1);
		int suvalineMees = pr6.Meetodid.suvalineT2isarvVahemikus(0, mehenimed.length - 1);
		int suvalineTegus6na = pr6.Meetodid.suvalineT2isarvVahemikus(0, tegus6nad.length - 1);

		// V�ljavalitud naise ja mehe nime j�rjekorra genereerimine
		// TEOSTUS: genereeritakse arv 0 v�i 1 ja valitakse j�rjekord
		// vastavalt 0: naine + teguss�na + mees v�i 1: mees + tegus�na + naine
		
		int suvlineNimi = pr6.Meetodid.suvalineT2isarvVahemikus(0, 1);

		String esimeneNimi;
		String teineNimi;

		if (suvlineNimi == 0) {
			esimeneNimi = naisenimed[suvalineNaine];
			teineNimi = mehenimed[suvalineMees];
		} else {
			esimeneNimi = mehenimed[suvalineMees];
			teineNimi = naisenimed[suvalineMees];
		}

		// Lause moodustamine
		System.out.println(esimeneNimi + " " + tegus6nad[suvalineTegus6na] + " " + teineNimi + ".");

	}
}
